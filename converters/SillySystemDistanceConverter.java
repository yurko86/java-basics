package converters;


/**
 * Write a description of class SillyMetricSystemDistanceConverter here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SillySystemDistanceConverter
{
    /**
     * 12 inches == 1 feet
     * 
     * @param  inches   how many inches we have
     * @return     how many feet does that make
     */
    public int inch2feet(int inches)
    {
        return inches/12;
    }
}
