package testinterface;

import java.util.Random;

/**
 *
 * @author Iurii Malkov implementation of card interface (testinterface.card)
 * with representation of card with int value
 */
public class CardInt implements Card {

    private int cardRank;

    @Override
    public String compareTo(Card other) {
        if (other.value() < this.value()) {
            return "higher";
        } else if (other.value() > this.value()) {
            return "lower";
        } else if (other.value() == this.value()) {
            return "equal";
        }
        return "";
    }

    @Override
    public int value() {
        return this.cardRank;
    }

    public CardInt(int val) {
        this.cardRank = val;
    }

    public static void main(String[] args) {

        Random generator = new Random();

        // create two cards with random value (1-52)
        CardInt card1 = new CardInt(generator.nextInt(51) + 1);
        CardInt card2 = new CardInt(generator.nextInt(51) + 1);

        System.out.println("card1->" + card1.value());
        System.out.println("card2->" + card2.value());

        System.out.println("card1 is " + card1.compareTo(card2) + " than card2");

    }

}
