package testinterface;

/**
 *
 * @author Iurii Malkov implementation of card interface (testinterface.card)
 * with representation of card with two string variables - Ranks and Suits
 */
public class CardsString implements Card {

    private String cardRank;
    private String cardSuit;

    public CardsString(String cardRank, String cardSuit) {
        this.cardRank = cardRank;
        this.cardSuit = cardSuit;
    }

    private int getRankValue() {
        switch (this.cardRank) {
            case "Two":
                return 1;
            case "Three":
                return 2;
            case "Four":
                return 3;
            case "Five":
                return 4;
            case "Six":
                return 5;
            case "Seven":
                return 6;
            case "Eight":
                return 7;
            case "Nine":
                return 8;
            case "Ten":
                return 9;
            case "Jack":
                return 10;
            case "Queen":
                return 11;
            case "King":
                return 12;
            case "Ace":
                return 13;
            default:
                return 0;
        }
    }

    private int getSuitValue() {
        switch (this.cardSuit) {
            case "Clubs":
                return 1;
            case "Diamonds":
                return 100;
            case "Hearts":
                return 10000;
            case "Spades":
                return 100000;
            default:
                return 0;
        }
    }

    public String toString() {
        return this.cardRank + " - " + this.cardSuit;
    }

    public static void main(String[] args) {
        
        //create two cards
        CardsString card1 = new CardsString("Ace", "Clubs");
        CardsString card2 = new CardsString("Five", "Diamonds");

        System.out.println("card1(" + card1.toString() + ")->" + card1.value());
        System.out.println("card2(" + card2.toString() + ")->" + card2.value());

        System.out.println("card1 is " + card1.compareTo(card2) + " than card2");

    }

    @Override
    public String compareTo(Card other) {
        if (other.value() < this.value()) {
            return "higher";
        } else if (other.value() > this.value()) {
            return "lower";
        } else if (other.value() == this.value()) {
            return "equal";
        }
        return "";
    }

    @Override
    public int value() {
        return this.getRankValue() * this.getSuitValue();
    }

}
