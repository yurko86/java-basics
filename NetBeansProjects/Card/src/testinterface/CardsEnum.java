package testinterface;

import java.util.Random;

/**
 *
 * @author Iurii Malkov implementation of card interface (testinterface.card)
 * with representation of card with two enums - Ranks and Suits
 */
public class CardsEnum implements Card {

    private Suits cardSuit;
    private Ranks cardRank;

    @Override
    public String compareTo(Card other) {
        if (other.value() < this.value()) {
            return "higher";
        } else if (other.value() > this.value()) {
            return "lower";
        } else if (other.value() == this.value()) {
            return "equal";
        }
        return "";

    }

    @Override
    public int value() {
        return this.getRankValue() * this.getSuitValue();
    }

    private int getSuitValue() {
        for (Ranks r : Ranks.values()) {
            if (cardRank == r) {
                return r.getValue();
            }
        }
        return 0;
    }

    private int getRankValue() {
        for (Suits s : Suits.values()) {
            if (cardSuit == s) {
                return s.getValue();
            }
        }
        return 0;
    }

    public CardsEnum(Ranks rank, Suits suit) {
        this.cardRank = rank;
        this.cardSuit = suit;

    }

    @Override
    public String toString() {
        return this.cardRank.toString() + " - " + this.cardSuit.toString();
    }

    public static void main(String[] args) {
        Random generator = new Random();
        // create two cards for test
        CardsEnum card1 = new CardsEnum(Ranks.Ace, Suits.Clubs);
        CardsEnum card2 = new CardsEnum(Ranks.Five, Suits.Diamonds);

        System.out.println("card1(" + card1.toString() + ")->" + card1.value());
        System.out.println("card2(" + card2.toString() + ")->" + card2.value());

        System.out.println("card1 is " + card1.compareTo(card2) + " than card2");
    }

}
