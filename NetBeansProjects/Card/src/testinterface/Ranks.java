package testinterface;
/**
 * 
 * @author Iurii Malkov
 * enum to represent card Rank and its value for card evaluation
 */
enum Ranks {

    Two(1),
    Three(2),
    Four(3),
    Five(4),
    Six(5),
    Seven(6),
    Eight(7),
    Nine(8),
    Ten(9),
    Jack(10),
    Queen(11),
    King(12),
    Ace(13);

    final int value;

    Ranks(int value) {
        this.value = value;
    }

    int getValue() {
        return this.value;
    }

}
