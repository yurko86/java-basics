package testinterface;

/**
 * 
 * @author Iurii Malkov
 * enum to represent card Suit and its value for card evaluation
 */
enum Suits {
    Clubs(1),
    Diamonds(100),
    Hearts(10000),
    Spades(100000);

    int value;

    Suits(int value) {
        this.value = value;
    }

    int getValue() {
        return this.value;
    }
}
