package testinterface;

import java.util.Random;

/**
 *
 * @author Iurii Malkov implementation of card interface (testinterface.card)
 * with representation of card with two int values - suit(1-4), rank (1-13)
 */
public class CardInts implements Card {

    private int rank;
    private int suit;

    public CardInts(int rank, int suit) {
        this.rank = rank;
        this.suit = suit;
    }

    @Override
    public String toString() {
        return ("rank:" + this.rank + "-suit:" + this.suit);
    }

    public static void main(String[] args) {
        Random generator = new Random();

        // create two cards with random value (1-52)
        CardInts card1 = new CardInts(generator.nextInt(12) + 1, generator.nextInt(3) + 1);
        CardInts card2 = new CardInts(generator.nextInt(12) + 1, generator.nextInt(3) + 1);

        System.out.println("card1(" + card1.toString() + ")->" + card1.value());
        System.out.println("card2(" + card2.toString() + ")->" + card2.value());

        System.out.println("card1 is " + card1.compareTo(card2) + " than card2");
    }

    @Override
    public String compareTo(Card other) {
        if (other.value() < this.value()) {
            return "higher";
        } else if (other.value() > this.value()) {
            return "lower";
        } else if (other.value() == this.value()) {
            return "equal";
        }
        return "";
    }

    @Override
    public int value() {
        /* multiply rank with next multipliers
        so possible value of kard will be
        Clubs (1)      ->  2     - 13
        Diamonds(100)  ->  200   - 1300
        Hearts(10000)  -> 20 000 - 130 000
        Spades(100000) -> 200 000- 1 300 000
         */

        switch (this.suit) {
            case 1:
                return (this.rank);
            case 2:
                return (this.rank * 100);
            case 3:
                return (this.rank * 10000);
            case 4:
                return (this.rank * 100000);
            default:
                return 0;
        }
    }

}
