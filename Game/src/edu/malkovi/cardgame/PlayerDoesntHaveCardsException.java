/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.malkovi.cardgame;

/**
 *
 * @author Administrator
 */
public class PlayerDoesntHaveCardsException extends Throwable {

    
    public PlayerDoesntHaveCardsException() {
    }

    public PlayerDoesntHaveCardsException(String msg) {
        super(msg);
    }
}
