package edu.malkovi.cardgame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Iurii Malkov
 */
public class CardsOnTable {

    Map<Card, Player> cardsOnTable = new HashMap<>();

    List<Player> selectedPlayers = new ArrayList<>();

    /**
     * Method to move card from player hand to table
     *
     * @param player player, who reveal card
     * @param card card, which was revealed
     */
    void takeCard(Player player, Card card) {
        cardsOnTable.put(card, player);
    }

    /**
     * Method to compare cards which players reveal
     *
     * @return list of players who have the most expensive card
     */
    List<Player> comparePlayersCards() {
        int maxCardValue = 0;
        System.out.println("comparing cards");

        // looping cards and try to find expensive one
        for (Map.Entry<Card, Player> pair : cardsOnTable.entrySet()) {
            if (maxCardValue == pair.getKey().getCardRankValue()) {
                selectedPlayers.add(pair.getValue());
            } else if (maxCardValue < pair.getKey().getCardRankValue()) {
                // if current card bigger then previous -> clear array
                selectedPlayers.clear();
                selectedPlayers.add(pair.getValue());
                maxCardValue = pair.getKey().getCardRankValue();
            } else if (0 == selectedPlayers.size()) {
                selectedPlayers.add(pair.getValue());
            }
        }
        return selectedPlayers;
    }

    /**
     * Method to return winner of round, or players for next duel
     *
     * @return Players list
     */
    List<Player> getPlayersWithBestCards() {
        return selectedPlayers;
    }

    /**
     * Method moves cards from table to player, who win a round
     *
     * @param player , which win round
     */
    void passCardsToPlayer(Player player) {

        for (Map.Entry<Card, Player> pair : cardsOnTable.entrySet()) {
            player.takeCard(pair.getKey());
        }
        cardsOnTable.clear();
    }

    /**
     * Method to show cards which players reveal
     */
    void showCardsOnTableByPlayer() {
        for (Map.Entry<Card, Player> pair : cardsOnTable.entrySet()) {
            System.out.println("Player " + pair.getValue().getPlayerName() + " put " + pair.getKey().toString());
        }
    }

    /**
     * Duel phase
     *
     * @return list of players, who have the biggest card
     */
    List<Player> runDuel(List<Player> selectedPlayers) throws PlayerDoesntHaveCardsException {

        System.out.println("----- run DUEL -----");

        Map<Card, Player> cardsOnTableHold = new HashMap<>();
        cardsOnTableHold.putAll(cardsOnTable);

        cardsOnTable.clear();

        //check players number of cards
        for (Player player : selectedPlayers) {
            if (player.getNumberOfPlayersCards() < 1) {

                // pass cards from duel to table 
                cardsOnTable.putAll(cardsOnTableHold);
                cardsOnTableHold.clear();

                throw new PlayerDoesntHaveCardsException(player.getPlayerName() + " doesn't have card for duel. returning cards back from table to Players");
            }
        }

        for (Player player : selectedPlayers) {
            takeCard(player, player.putCardOnTable());
        }

        showCardsOnTableByPlayer();

        selectedPlayers = comparePlayersCards();

        // pass cards from duel to table 
        cardsOnTable.putAll(cardsOnTableHold);

        cardsOnTableHold.clear();

        return selectedPlayers;
    }

    /**
     * method to return cards back to user in case when in duel player have the
     * same value for a card and cant continue game because of absence of cards
     */
    void passCardsBackToPlayers() {
        System.out.println("some of Players doesn't have cards. returning all cards back to players");
        for (Map.Entry<Card, Player> pair : cardsOnTable.entrySet()) {

            pair.getValue().takeCard(pair.getKey());
        }
        cardsOnTable.clear();
    }

}
