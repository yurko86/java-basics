package edu.malkovi.cardgame;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author Iurii Malkov
 */
public class GameUtils {

    static int askNumberOfPlayers(int minCountOfPlayers, int maxCountOfPlayers) {
        int result = -1;
        Scanner s = new Scanner(System.in);

        while (result < 0) {
            System.out.print("Please enter number of players: ");
            try {
                result = validatePlayersCount(s.nextInt(), minCountOfPlayers, maxCountOfPlayers);

            } catch (InputMismatchException e) {
                s.nextLine();
                System.err.println("Incorect value. Try again");
            }

        }
        s.close();
        return result;
    }

    static int validatePlayersCount(int numberOfPlayers, int lowerBound, int upperBound) {
        if (lowerBound < numberOfPlayers && numberOfPlayers < upperBound) {
            return numberOfPlayers;
        } else {
            System.err.println("Number of players should be in interval 1-52!");
            return -1;

        }
    }

}
