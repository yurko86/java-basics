package edu.malkovi.cardgame;

/**
 * Game card object for game with 52 cards Implemented with Enums for rank and
 * suit
 *
 * @author Iurii Malkov
 */
public class RegularDeckCard implements Card {

    private CardSuit suit;
    private CardRank rank;

    @Override
    public String toString() {
        return rank.toString() + " - " + suit.toString();
    }
    
    @Override
    public int getCardRankValue(){
        return rank.getValue();
    }

    RegularDeckCard(CardSuit suit, CardRank rank) {
        this.suit = suit;
        this.rank = rank;
    }
}
