package edu.malkovi.cardgame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Implementation of deck for War game
 *
 * @author Iurii Malkov
 */
public class WarDeck implements Deck {

    private List<Card> deck = new ArrayList<>();

    public WarDeck() {
        initializeDeck();
    }

    @Override
    public List<Card> shuffle() {
        Collections.shuffle(this.deck);
        return this.deck;
    }

    @Override
    public Card dealACard(String strategy) {
        switch (strategy) {
            case "up": {
                return deck.remove(0);
            }
            case "down": {
                return deck.remove(deck.size() - 1);
            }
            default: {
                Random generator = new Random();
                return deck.remove(generator.nextInt(deck.size() - 1));
            }
        }
    }

    @Override
    public List<Card> initializeDeck() {
        System.out.println("---------------------------");
        System.out.println("Start creating the deck");
        for (CardSuit cs : CardSuit.values()) {
            for (CardRank cr : CardRank.values()) {
                deck.add(new RegularDeckCard(cs, cr));
//!!!!!                System.out.println("added card (" + cr.toString() + "-" + cs.toString() + ") ");
            }
        }
        System.out.println("Deck is succesfully created");
        Collections.shuffle(deck);
        System.out.println("Deck is shuffled");
        System.out.println("---------------------------");
        return deck;
    }

    @Override
    public void initialDealing(List<Player> playersList) {
        System.out.println("---------initial dealing-----------------");
        while (deck.size() > 0) {
            playersList.get(deck.size() % playersList.size()).takeCard(this.dealACard("down"));
        }
        System.out.println("initial dealing finished. deck has " + deck.size() + " cards");
    }
}
