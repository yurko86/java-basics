package edu.malkovi.cardgame;

import java.util.List;

/**
 * behavior of deck that must implement by decks for appropriate games
 *
 * @author Iurii Malkov
 */
public interface Deck {

    /**
     * Method shuffles the deck
     */
    public List<Card> shuffle();

    /**
     * Method delete card from deck and returns it
     *
     * @return Card
     */
    public Card dealACard(String strategy);

    /**
     * Method prepares deck for concrete game. initialize number of cards etc.
     *
     * @param game
     */
    public List<Card> initializeDeck();

    /**
     * Method do initial dealing for game
     *
     * @param playersList
     */
    public void initialDealing(List<Player> playersList);

}
