package edu.malkovi.cardgame;

/**
 * Enum for regular deck card's ranks
 *
 * @author Iurii Malkov
 */
enum CardRank {
    Two(1),
    Three(2),
    Four(3),
    Five(4),
    Six(5),
    Seven(6),
    Eight(7),
    Nine(8),
    Ten(9),
    Jack(10),
    Queen(11),
    King(12),
    Ace(13);

    //cost of card for comparing
    final int cost;

    CardRank(int cost) {
        this.cost = cost;
    }

    // return value of card in int
    int getValue() {
        return this.cost;
    }

}
