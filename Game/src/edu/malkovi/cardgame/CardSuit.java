package edu.malkovi.cardgame;

/*
Enum for regular deck card's suits
*/
enum CardSuit
{
    Clubs,
    Diamonds,
    Hearts,
    Spades
}