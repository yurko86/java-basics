package edu.malkovi.cardgame;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of player for War Game
 *
 * @author Iurii Malkov
 */
public class WarPlayer implements Player {

    private List<Card> playersCards = new ArrayList();
    private String name;

    /**
     * return all cards which belong to player
     *
     * @return List of Card
     */
    WarPlayer(String name) {
        this.name = name;
    }

    public List<Card> getPlayersCards() {
        return playersCards;
    }

    @Override
    public void takeCard(Card card) {
        this.playersCards.add(card);
    }

    @Override
    public Card putCardOnTable() {
        Card card = playersCards.get(0);
        playersCards.remove(card);
        return card;
    }

    @Override
    public String getPlayerName() {
        return name;
    }

    @Override
    public int getNumberOfPlayersCards() {
        return playersCards.size();
    }

}
