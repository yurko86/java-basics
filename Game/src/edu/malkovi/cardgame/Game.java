package edu.malkovi.cardgame;

import java.util.List;

/**
 * interface for card game
 * @author Iurii Malkov
 */
public interface Game {
   
    /**
     * Method creates players
     * @return list of created players
     */    
    public List<Player> createPlayers();

    
    /**
     * Method starts game 
     */
    public void startGame();
}
