package edu.malkovi.cardgame;

/**
 * interface of Card for card game
 *
 * @author Iurii Malkov
 */
public interface Card {

    public String toString();
    
    public int getCardRankValue();
}
