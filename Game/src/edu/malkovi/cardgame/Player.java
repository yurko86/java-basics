package edu.malkovi.cardgame;

import java.util.List;

/**
 * Interface for player of card games
 *
 * @author Iurii Malkov
 */
public interface Player {

    /**
     * Method to take card and put it players hand
     *
     * @param card
     */
    public void takeCard(Card card);

    /**
     * Method removed card from players hand and put it to table
     *
     * @return
     */
    public Card putCardOnTable();

    /**
     * Method to get list of all players cards
     *
     * @return list of Cards
     */
    public List<Card> getPlayersCards();

    public String getPlayerName();
    
    public int getNumberOfPlayersCards();
}
