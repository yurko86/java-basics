package edu.malkovi.cardgame;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

/**
 * implementation of Game for War Game
 *
 * @author Iurii Malkov
 */
public class WarGame implements Game {

    final static int MIN_NUMBER_OF_PLAYERS = 2;
    final static int MAX_NUMBER_OF_PLAYERS = 52;
    
    
    List<Player> listOfPlayers = new ArrayList<>();

    public static void main(String[] args) {
        Game warGame = new WarGame();
    }

    public WarGame() {
        new WarDeck().initialDealing(createPlayers());
        showPlayersCards();
        startGame();
    }

    /**
     * Output all players as well as their cards
     */
    void showPlayersCards() {
        int i = 1;
        System.out.println("---------------------------");
        for (Player p : listOfPlayers) {
            System.out.println("player " + p.getPlayerName() + " has cards:");
            System.out.println("---------------------------");
            for (Card card : p.getPlayersCards()) {
                System.out.println(card.toString());
            }
            i++;
            System.out.println("---------------------------");
        }
    }

    /**
     * Method display number of cards which belong to user
     */
    void showPlayersCardsNumber() {
        int sum = 0;
        for (Player player : listOfPlayers) {
            System.out.println(player.getPlayerName() + " has " + player.getNumberOfPlayersCards() + " cards");
            sum += player.getNumberOfPlayersCards();
        }
    }

    @Override
    public List<Player> createPlayers() {
        int numberOfPlayers = -1;

        //while (numberOfPlayers < 0) {
        //    numberOfPlayers = GameUtils.validatePlayersCount(GameUtils.askNumberOfPlayers());
        //}
        numberOfPlayers = GameUtils.askNumberOfPlayers(MIN_NUMBER_OF_PLAYERS, MAX_NUMBER_OF_PLAYERS);

        System.out.println("---------------------------");

        for (int i = 1; i <= numberOfPlayers; i++) {
            listOfPlayers.add(new WarPlayer("Player " + i));
        }

        System.out.println("created " + numberOfPlayers + " player(s)");
        System.out.println("---------------------------");

        return listOfPlayers;
    }

    @Override
    public void startGame() {
        boolean exitFlag = false;

        System.out.println("--------game begin---------");
        CardsOnTable cardsOnTable = new CardsOnTable();
        int round = 1;

        while (!exitFlag) {
            System.out.println("---------------------------");
            System.out.println("round->" + round);
            System.out.println("---------------------------");

            for (Player player : listOfPlayers) {
                cardsOnTable.takeCard(player, player.putCardOnTable());
            }

            cardsOnTable.showCardsOnTableByPlayer();
            try {
                if (1 < cardsOnTable.comparePlayersCards().size()) {

                    //running duel till we have 1 winner
                    while (cardsOnTable.runDuel(cardsOnTable.getPlayersWithBestCards()).size() > 1) {
                        //
                    }
                    System.out.println("Duel won " + cardsOnTable.getPlayersWithBestCards().get(0).getPlayerName());
                } else {
                    System.out.println("Round won " + cardsOnTable.getPlayersWithBestCards().get(0).getPlayerName());
                }

                cardsOnTable.passCardsToPlayer(cardsOnTable.getPlayersWithBestCards().get(0));
                showPlayersCardsNumber();
                removePlayersWith0Cards(listOfPlayers);

                exitFlag = checkForWinner(listOfPlayers);
                round++;
            } catch (PlayerDoesntHaveCardsException e) {
                cardsOnTable.passCardsBackToPlayers();
                showPlayersCardsNumber();
            }
        }
    }

    /**
     * Method to remove players w/o cards safe removal with iterator
     *
     * @param listOfPlayers
     */
    private void removePlayersWith0Cards(List<Player> listOfPlayers) {
        Iterator<Player> iterator = listOfPlayers.iterator();
        while (iterator.hasNext()) {
            Player player = iterator.next();
            if (0 == player.getPlayersCards().size()) {
                System.err.println("Player " + player.getPlayerName() + " was removed from game (doesnt have cards)");
                iterator.remove();
            }
        }
    }

    /**
     * Method to check for winner
     *
     * @param listOfPlayers
     * @return true if game has a winner (1 player with a cards)
     */
    private boolean checkForWinner(List<Player> listOfPlayers) {
        if (1 == listOfPlayers.size()) {
            System.out.println("---------------------------");
            System.out.println("winner is -> " + listOfPlayers.get(0).getPlayerName());
            System.out.println("---------------------------");
            return true;
        } else {
            return false;
        }
    }

}
