------------------------------------------------------------------------
This is the project README file. Here, you should describe your project.
Tell the reader (someone who does not know anything about this project)
all he/she needs to know. The comments should usually include at least:
------------------------------------------------------------------------

PROJECT TITLE: Java Basics, BlueJ IDE hands-on
PURPOSE OF PROJECT: To demonstrate how to use BlueJ IDE starting with hand-coding a simple calculator. To later demonstrate OO principles by preparing components for card games, starting with Card Dealer.
VERSION or DATE: April 2017
HOW TO START THIS PROJECT: Open it in BlueJ IDE, then invoke methods of class Calculator (or others).
AUTHORS: TJB
USER INSTRUCTIONS: all should be demonstrated during the course, refer to README.txt in each package 
for details and current instructions.

You will learn here:
- how to create an object in BlueJ
- how such an object looks like
- that for now, you do not need instance variables or constructors
- that there are three types of comments in Java
- how to declare a method in Java class
- how to make a method return something
- about `int` datatype
- how to use simple math in Java (arithmetic operations)
- how to operate on object in BlueJ (create, use, invoke methods)

More? Done here? Go to converters package.
