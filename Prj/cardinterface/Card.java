package cardinterface;

/**
 * @author LAFK_pl, Tomasz.Borek@gmail.com
 */

public interface Card {

    /**
     * Simple card comparison.
     *
     * @param other another card to compare to
     * @return "Higher is: higher card goes here" or "Draw on: tied card goes here".
     */

    String compareTo(Card other);


    /**
     * Flattens the card down to a number
     * @return a Card as an int
     */

    int value();

}

