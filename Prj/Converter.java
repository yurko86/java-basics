
/**
 * Write a description of class Converter here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Converter
{
    // instance variables - replace the example below with your own
    private int x;

    /**
     * Constructor for objects of class Converter
     */
    public Converter()
    {
        // initialise instance variables
        x = 0;
    }

    /**
     * An example of a method - replace this comment with your own
     *
     * @param  y  a sample parameter for a method* @return    the sum of x and y
     */
    public double convertCmToIn(int val)
    {
        //1 inch  = 2.54 cm
        return val/2.54;
    }
    
    public double convertMmToIn(int val)
    {
        //1 inch  = 2.54 cm
        return convertCmToIn(val)/10;
    }
    
    public double convertMToIn(int val)
    {
        //1 inch  = 2.54 cm
        return convertCmToIn(val)*100;
    }
    
    public double convertKmToIn(int val)
    {
        //1 inch  = 2.54 cm
        return convertCmToIn(val)*100*1000;
    }

    
    /////////////////////
    
    
    public double convertCmToFt(int val)
    {
        //1 inch  = 2.54 cm
        return convertCmToIn(val)*3;
    }
    
    public double convertMmToFt(int val)
    {
        //1 inch  = 2.54 cm
        return convertMmToIn(val)*3;
    }
    
    public double convertMToFt(int val)
    {
        //1 inch  = 2.54 cm
        return convertMToIn(val)*3;
    }
    
    public double convertKmToFt(int val)
    {
        //1 inch  = 2.54 cm
        return convertKmToIn(val)*3;
    }

    ///////////////////////
    
    public double convertCmToY(int val)
    {
        //1 inch  = 2.54 cm
        return convertCmToFt(val)*12;
    }
    
    public double convertMmToY(int val)
    {
        //1 inch  = 2.54 cm
        return convertMmToFt(val)*12;
    }
    
    public double convertMToY(int val)
    {
        //1 inch  = 2.54 cm
        return convertMToFt(val)*12;
    }
    
    public double convertKmToY(int val)
    {
        //1 inch  = 2.54 cm
        return convertKmToFt(val)*12;
    }
    
    ////////////////
    
     
    public double convertCmToMi(int val)
    {
        //1 inch  = 2.54 cm
        return convertCmToY(val)*1560;
    }
    
    public double convertMmToMi(int val)
    {
        //1 inch  = 2.54 cm
        return convertMmToFt(val)*1560;
    }
    
    public double convertMToMi(int val)
    {
        //1 inch  = 2.54 cm
        return convertMToFt(val)*1560;
    }
    
    public double convertKmToMi(int val)
    {
        //1 inch  = 2.54 cm
        return convertKmToFt(val)*1560;
    }
    
    
    ////////////////////////////////////////////////////////////////////// back approach
    
public double convertInToCm(int val)
    {
        //1 inch  = 2.54 cm
        return val*2.54;
    }

 public double convertInToMm(int val)
    {
        //1 inch  = 2.54 cm
        return convertInToCm(val)/10;
    }
    
    public double convertInToM(int val)
    {
        //1 inch  = 2.54 cm
        return convertInToCm(val)*100;
    }
    
    public double convertInToKm(int val)
    {
        //1 inch  = 2.54 cm
        return convertInToCm(val)*100*1000;
    }

    /////////////////////
    
    
    public double convertFtToCm(int val)
    {
        //1 inch  = 2.54 cm
        return convertInToCm(val)*3;
    }
    
    public double convertFtToMm(int val)
    {
        //1 inch  = 2.54 cm
        return convertInToMm(val)*3;
    }
    
    public double convertFtToM(int val)
    {
        //1 inch  = 2.54 cm
        return convertInToM(val)*3;
    }
    
    public double convertFtToKm(int val)
    {
        //1 inch  = 2.54 cm
        return convertInToKm(val)*3;
    }

    
     /////////////////////
    
    
    public double convertYToCm(int val)
    {
        //1 inch  = 2.54 cm
        return convertFtToCm(val)*12;
    }
    
    public double convertYToMm(int val)
    {
        //1 inch  = 2.54 cm
        return convertFtToMm(val)*12;
    }
    
    public double convertYToM(int val)
    {
        //1 inch  = 2.54 cm
        return convertFtToM(val)*12;
    }
    
    public double convertYToKm(int val)
    {
        //1 inch  = 2.54 cm
        return convertFtToKm(val)*12;
    }

     /////////////////////
    
    
    public double convertMiToCm(int val)
    {
        //1 inch  = 2.54 cm
        return convertYToCm(val)*1560;
    }
    
    public double convertMiToMm(int val)
    {
        //1 inch  = 2.54 cm
        return convertYToMm(val)*1560;
    }
    
    public double convertMiToM(int val)
    {
        //1 inch  = 2.54 cm
        return convertYToM(val)*1560;
    }
    
    public double convertMiToKm(int val)
    {
        //1 inch  = 2.54 cm
        return convertYToKm(val)*1560;
    }


}
