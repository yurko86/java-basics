package Converters;

import java.math.BigDecimal; 

public class Convertet
{
    double getMultiplier(String x){
                  
        for(FromMetric m : FromMetric.values()) {     
            if(x.equals( m.name())){
                return m.getValue();
            }
        }
    
        return  0;
    } 
        
    public String convertValue(String fromMeasure, String toMeasure, double value){
        BigDecimal result = new BigDecimal(0);
        
        double multiplier1 = getMultiplier(fromMeasure);
        System.out.println("from " + fromMeasure);
        System.out.println(multiplier1);
        
        double multiplier2 = getMultiplier(toMeasure);
        System.out.println("to " + toMeasure);
        System.out.println(multiplier2);
        
        if(multiplier1 == 0 || multiplier2 ==0){
            System.out.println("wrong input value for measure(s)");
            return "0";
        }
        else {
        
        result = new BigDecimal(multiplier1).multiply( new BigDecimal(multiplier2 * value));
        System.out.println("result -> "+result);
        return result.setScale(5, BigDecimal.ROUND_CEILING).toString();
        } 
    }
   
}
