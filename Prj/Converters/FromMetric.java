package Converters;

public enum FromMetric
{
   MM(1),
   CM(10),
   M(1000),
   KM(1000000),
   IN(0.039370078740157),
   FT(0.0032808398950131),
   YD(0.0010936132983377),
   MI(0.000000621371192237)
   ;
   

   private final double value;
   FromMetric (double value){
    this.value = value;
    }
    
    double getValue() {
      return value;
   } 
}
