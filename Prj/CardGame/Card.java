package CardGame;

class Card
{
    private String rank;
    private String suit;
   
    Card(String rank, String suit )
    {
        this.rank = rank;
        this.suit = suit;
    }

   
    int compare(Card other){
        
        int card1Value =  getCardRankValue(this.rank) * getSuitValue(this.suit);
        int card2Value = getCardRankValue(other.getRank()) * getSuitValue(other.getSuit());
        
        if (card1Value>card2Value){
            return 1;
        } else if (card1Value<card2Value){
            return -1;
        } else {return 0;}
        
    }
    
    
    int getCardRankValue(String cardValue){
        for(CardRank cr: CardRank.values()){
            if (cr.toString() == cardValue) {
                return cr.getValue();
            }
        }
        return 0;
    }
    
    int getSuitValue(String value){
        switch(value){
            case "Diamonds": return 1;
            case "Clubs":    return 2;
            case "Hearts":   return 3;
            case "Spades":   return 4;
            default: return 0;
        }
    }
    
    String getRank(){
        return this.rank;
    }
    
    String getSuit(){
        return this.suit;
    }
    

}
