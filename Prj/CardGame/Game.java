package CardGame;


import java.util.ArrayList;
import java.util.List;


class Game
{
    private static Deck deck = new Deck();
    private List<Player> playersList = new ArrayList<>();
    
    private String dealingStrategy;
    
    
    void Game(List<Player> playersList, String gameName){
        
        System.out.println("--------------begin-game-------------------------------");
        
        dealingStrategy = "Random";
        System.out.println("we play           -> "+gameName);
        System.out.println("number of players -> "+playersList.size());
        System.out.println("dealing Strategy  -> "+dealingStrategy);
        
        for (Player p : playersList){
          p.takeCard(deck.getCardFromDeck(dealingStrategy)); 
        }
      
        
    }
}
