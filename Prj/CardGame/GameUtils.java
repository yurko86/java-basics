package CardGame;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;

public class GameUtils
{
    
        
    
    
    /**
     * class constructor  
     */
    GameUtils(int numOfPlayers){
        
        List<Player> playerList = createPlayers(numOfPlayers);
        System.out.println("-------------------------------");
        getListOfAvailableGames(playerList);
        System.out.println("-------------------------------");
        getListOfNotAvailabeGames(playerList);
        System.out.println("-------------------------------");
        
        
    }
    
    
    /**
    * create list of player objects
    * @param numOfPlayers the number of players
    */
    
    List<Player> createPlayers(int numOfPlayers){
        
        List<Player> playersList = new ArrayList<>();
        
         for (int i=1; i<= numOfPlayers; i++){
            playersList.add(new Player());           
         }
         System.out.println("added " + numOfPlayers + " players");
         
         return playersList;
    }
    
    /**
     * return age of youngest player in list
     * @param playersList list of players 
     */
    int getMinPlayersAge(List<Player> playersList){
        int minAge = 100;
        
        for (Player p : playersList){
            if(minAge>p.getAge()){
                minAge = p.getAge();
            }
        }
        System.out.println("min age ->"+ minAge);
        return minAge;
    }

    /**
     * return age category based on input age
     * @param minAge age of person
     */
    int getAgeCategory(int minAge){
        int ageCategory=0;
        for (AgeCategory ac : AgeCategory.values()){
            if (ac.evaluate(minAge)!=0){
                ageCategory = ac.evaluate(minAge);
                break;
            }
        }
        System.out.println("age category ->"+ ageCategory);
        return ageCategory;
    }
    
    
    
    /**
     * checks players array and return list of players
     */
    int getNumberOfPlayers(List<Player> playersList){
        if(playersList == null){
            return  0;
        } else if (playersList.size() < 2){
              return  0; 
        } else if (playersList.size() > 4){
            return 4;
        } else return playersList.size();
    }
    
    
    
    /**
     * return list of game which are available to play
     * 
     */
    Set<String> getListOfAvailableGames(List<Player> playersList){

        int ageCategory = getAgeCategory(getMinPlayersAge(playersList));
        int numberOfPlayers = getNumberOfPlayers(playersList);
        Set<String> gamesSet = new HashSet<>();
        
        String result;
        
        for (Games g : Games.values()){
            result = g.evaluate(numberOfPlayers, ageCategory, true);
            if (result != null) {
                gamesSet.add(result);
                System.out.println("can play -> "+result);
            }
        }
        return gamesSet; 
    }

     /**
     * return list of game which are available to play
     */
    
    Set<String>  getListOfNotAvailabeGames(List<Player> playersList){

        int ageCategory = getAgeCategory(getMinPlayersAge(playersList));
        int numberOfPlayers = getNumberOfPlayers(playersList);
        Set<String> gamesSet = new HashSet<>();
        
        String result;
        
        for (Games g : Games.values()){
            result = g.evaluate(numberOfPlayers, ageCategory, false);
            if (result != null) {
                gamesSet.add(result);
                System.out.println("CAN'T play -> "+result);
            }
        }
        return gamesSet;     
       
    }
}
