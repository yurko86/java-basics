package CardGame;

import java.util.ArrayList;
import java.util.Random;
import java.util.List;
import java.util.Collections;

class Deck
{
    // instance variables - replace the example below with your own
    private List<Card> cardList = new ArrayList<>();

    
    Deck()
    {
      // create deck of cards
        
      for(CardSuit cs : CardSuit.values() ){
        for(CardRank cr : CardRank.values()){
            Card card = new Card(cs.name(), cr.name());
            cardList.add(card);
        }
      }
    }

    
    Card getCardFromDeck(String dealingStrategy){
       
        int cardNumber;
        
        switch (dealingStrategy){
           case "Random": 
                Random generator = new Random();
                cardNumber = generator.nextInt(cardList.size());
                break;
           case "Top":
                cardNumber = 1;
                break;
           case "Bottom":
                cardNumber = cardList.size();
                break;
           default: cardNumber = 1;
        }
        return  cardList.remove(cardNumber);
    }
    
    
    void shuffleDeck(){
        Collections.shuffle(cardList);
    }
    
    int getCount(){
            
        System.out.println("card list size "+cardList.size());
        return cardList.size();
    }

}
