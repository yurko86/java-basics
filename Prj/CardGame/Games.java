package CardGame;

public enum Games
{
   //max number of players | age_group 1 | age_group 2 | age_group 3 
      
Old_Maid   (2, 1,0,1, "Old Maid"), 
War        (2, 1,0,1, "War"), 
Gypsy      (2, 1,1,1, "Gypsy"),
Makao      (2, 1,1,1, "Makao"),
Remi_Bridge(2, 1,1,1, "Remi Bridge"),
Thief      (4, 1,1,1, "Thief"),
Canasta    (3, 0,1,1, "Canasta"),
Bridge     (4, 0,1,1, "Bridge"),
Wist       (4, 0,1,1, "Wist"),
Poker      (2, 0,0,1, "Poker");

  private final int numberOfPlayers;
  private final int ageCategory1;
  private final int ageCategory2;
  private final int ageCategory3;
  private final String gameName;

  
     Games(int minNumberOfPlayers, int ageCategory1,int ageCategory2,int ageCategory3, String gameName) {
            this.numberOfPlayers = minNumberOfPlayers;
            this.ageCategory1 = ageCategory1;
            this.ageCategory2 = ageCategory2;
            this.ageCategory3 = ageCategory3;
            this.gameName = gameName;
        }

     String evaluate(int numberOfPlayers, int ageCategory, boolean mode){
       
        if (mode){ // search for available gemes
            if (numberOfPlayers == this.numberOfPlayers) {
                switch (ageCategory){
                    case 1: if (this.ageCategory1==1){return this.gameName;} else {return null;}
                    case 2: if (this.ageCategory2==1){return this.gameName;} else {return null;}
                    case 3: if (this.ageCategory3==1){return this.gameName;} else {return null;}
                    default: return null;
                }    
            }
           return null;
        } else { // search for not available games
            if (numberOfPlayers != this.numberOfPlayers) {
                return this.gameName;
            } else {
                switch (ageCategory){
                    case 1: if (this.ageCategory1!=1){return this.gameName;} else {return null;}
                    case 2: if (this.ageCategory2!=1){return this.gameName;} else {return null;}
                    case 3: if (this.ageCategory3!=1){return this.gameName;} else {return null;}
                    default: return null;
                }
            }
            
          }
     }
        
     
    
}
