package CardGame;

public enum AgeCategory
{
    till_10   (1,10),
    before_18 (2,17),
    adult     (3,100);

    private final int age;
    private final int categoryNumber;

    AgeCategory(int categoryNumber,int age){
        this.age = age;
        this.categoryNumber = categoryNumber;
    }   

    int evaluate(int age){
       
        if (age <= this.age) {
            return this.categoryNumber;
        }
      
         return 0;
    }


}
