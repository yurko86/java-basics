1) Please implement a simple Card Dealer, OO-style.

OO-style, meaning "concepts are modelled as objects".

Requirements:

1. For now, up to 4 players
2. Normal deck of cards (52 cards, no jokers)
3. Cards are from 2 - Ace, colours are Clubs ♣, Diamonds ♢, Hearts ♡ and Spades ♠ 
4. For now I'll be satisfied if dealer can deal one card to each player.
5. Cards dealt cannot still be in the deck.
6. Card Dealer should have "shuffle" method, which shuffles the deck (changes it's order).

Reqs change: number of players is now 4, I want the shuffle functionality. 

In future, I envision:

- various amount of players
- many dealing strategies 
- and possibly more




2) Please implement a program, that takes into account array of Players (so amount 
of players and their age), and returns a list of game that WILL NOT be played, based on:

PLAYERS | GAMES
2       | Old Maid, War, Gypsy, Makao, Poker, Remi-Bridge
3       | Old Maid, War, Gypsy, Makao, Poker, Remi-Bridge, Canasta
4       | Old Maid, War, Gypsy, Makao, Poker, Remi-Bridge, Canasta, Bridge, Wist, Thief
more    | as above

AGE RANGE | GAMES
<= 10     | Old Maid, War, Gypsy, Makao, Remi-Bridge, Thief
10 - 18   | Gypsy, Makao, Remi-Bridge, Canasta, Bridge, Wist, Thief NEVER Old Maid, ALMOST NEVER War
>= 18     | Old Maid, War, Gypsy, Makao, Poker, Remi-Bridge, Canasta, Bridge, Wist, Thief

Program should accept array of Player objects and return array of Games that cannot be played 
due to criteria above.

In future:

0. full mappings (shuffling strategies)
1. legal age (games for adults)
2. legal age changes per place (location property configured)


3) Different implementations of a Card and it's comparison

Please create four classes implementing one common interface Card (see it in this package). Implement methods value and compareTo.

Feel free to use ANY IDE YOU WANT.

CardOnInt - one integer between 1-52
CardOnInts - one int for card rank, one for card colour
CardViaEnums - enums, CardSuit and CardRank
Your Own Implementation:
two Strings = Iurii
nothing else cause he was absent = Roland (and those who were absent)
String for colour, int for rank = Iryna
two chars = Mateusz
double between <1.0, 4.99> = Aleksandr
two classes = Sergey
