package cards.core;


public class ArrayCreation
{
    
    class Player {
        int age;
    
        Player() {}
        
        Player(int age) {
            this.age = age;
        }
    }
    
    

    private int[] x = {1,2,3};
    private Integer[] x1= {1,new Integer(2), Integer.valueOf(3)};
    private Player[] p = {new Player(5)};
    private Object[] o;

    void operateOnArrays() {
        o = new Object[]{new Object(), new Object(), new Object()};
        Player one = p[10];
        
        for (Player player : p) {
            p.getAge();
        }
        
    }
}
